//
// Created by valeness on 4/6/18.
//

#ifndef ROBIT_ROBOT_H
#define ROBIT_ROBOT_H


#include "Motor.h"

class Robot {

    public: Motor *leftMotor;
    public: Motor *rightMotor;

    unsigned long turn_time = 350;

    void turnLeft() {
        leftMotor->backward(150);
        rightMotor->forward(150);
        delay(turn_time);
        stop();
    }

    void turnRight() {
        leftMotor->forward(150);
        rightMotor->backward(150);
        delay(turn_time);
        stop();
    }


    void forward() {
        leftMotor->forward(190);
        rightMotor->forward(190);
        delay(400);
        stop();
    }

    void backward() {
        leftMotor->backward(150);
        rightMotor->backward(150);
        delay(400);
        stop();
    }

    void stop() {
        leftMotor->reset();
        rightMotor->reset();
    }

};


#endif //ROBIT_ROBOT_H

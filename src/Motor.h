//
// Created by valeness on 4/6/18.
//

#include "Arduino.h"

#ifndef ROBIT_MOTOR_H
#define ROBIT_MOTOR_H


class Motor {

    int pin1;
    int pin2;

    public:
        Motor(int motorpin1, int motorpin2) {
            pin1 = motorpin1;
            pin2 = motorpin2;
        }

    void reset() {
        digitalWrite(pin1, LOW);
        digitalWrite(pin2, LOW);

        analogWrite(pin1, 0);
        analogWrite(pin2, 0);
    }

    void forward(int speed = 120) {
        reset();
        digitalWrite(pin1, HIGH);
        analogWrite(pin2, speed);
    }

    void backward(int speed = 120) {
        reset();
        digitalWrite(pin2, HIGH);
        analogWrite(pin1, speed);
    }

};


#endif //ROBIT_MOTOR_H

#include "Arduino.h"
#include "Motor.h"
#include "Robot.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "environment.h"

int rightMotorIn1 = 12;
int rightMotorIn2 = 14;

int leftMotorIn1 = 13;
int leftMotorIn2 = 15;

Motor *leftMotor;
Motor *rightMotor;
Robot *robot;

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup_wifi() {

    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    randomSeed(micros());

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    for (int i = 0; i < length; i++) {
        Serial.print((char)payload[i]);
    }
    Serial.println();

    // Switch on the LED if an 1 was received as first character
    if ((char)payload[0] == 'l') {
        robot->turnLeft();
    } else if((char) payload[0] == 'r') {
        robot->turnRight();
    } else if((char) payload[0] == 'f') {
        robot->forward();
    } else if((char) payload[0] == 'b') {
        robot->backward();
    }

}


void reconnect() {
    // Loop until we're reconnected
    while (!client.connected()) {
        Serial.print("Attempting MQTT connection...");
        // Create a random client ID
        String clientId = "ESP8266Client-";
        clientId += String(random(0xffff), HEX);
        // Attempt to connect
        if (client.connect(clientId.c_str())) {
            Serial.println("connected");
            // Once connected, publish an announcement...
            client.publish("outTopic", "hello world");
            // ... and resubscribe
            client.subscribe("inTopic");
        } else {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" try again in 5 seconds");
            // Wait 5 seconds before retrying
            delay(5000);
        }
    }
}

void setup() {
    // initialize LED digital pin as an output.
    pinMode(rightMotorIn1, OUTPUT);
    pinMode(rightMotorIn2, OUTPUT);

    pinMode(leftMotorIn1, OUTPUT);
    pinMode(leftMotorIn2, OUTPUT);

    leftMotor = new Motor(leftMotorIn1, leftMotorIn2);
    rightMotor = new Motor(rightMotorIn1, rightMotorIn2);

    robot = new Robot();
    robot->leftMotor = leftMotor;
    robot->rightMotor = rightMotor;

    Serial.begin(115200);
    setup_wifi();
    client.setServer(mqtt_server, 1885);
    client.setCallback(callback);

}

void loop() {

    if (!client.connected()) {
        reconnect();
    }
    client.loop();



//    long now = millis();
//    if (now - lastMsg > 2000) {
//        lastMsg = now;
//        ++value;
//        snprintf (msg, 75, "hello world #%ld", value);
//        Serial.print("Publish message: ");
//        Serial.println(msg);
//        client.publish("outTopic", msg);
//    }

//    robot->turnLeft();
//
//    delay(1000);
//
//    robot->turnRight();
//
//    delay(5000);
}